var url = "";
let vrView;
let gallery = [];

let debounce;

let localStorageEnabled;

function storageAvailable(type) {
    let storage;
    try {
        storage = window[type];
        const x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    }
    catch(e) {
        return e instanceof DOMException && (
            // everything except Firefox
            e.code === 22 ||
            // Firefox
            e.code === 1014 ||
            // test name field too, because code might not be present
            // everything except Firefox
            e.name === 'QuotaExceededError' ||
            // Firefox
            e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
            // acknowledge QuotaExceededError only if there's something already stored
            (storage && storage.length !== 0);
    }
}

window.addEventListener('load', () => {
    $("#url").val("");
    const search = (new URL(window.location.href)).searchParams;
    localStorageEnabled = storageAvailable('localStorage') && !search.has("g");
    if(localStorageEnabled) {
        let cookiesAllowed = !!localStorage.getItem("cookies");
        if(!cookiesAllowed) {
            $("#cookieToast").toast("show");
            localStorageEnabled = false;
        }
        else {
            gallery = localStorage.getItem("gallery") ? localStorage.getItem("gallery").split(',') : [];
    
            gallery.forEach(x => addToGallery(x, true));
    
            url = localStorage.getItem("url");
            if(!url && gallery.length) url = gallery[0];
            $("#url").val(url);
        }
    }
    else if(search.has("g")) {
        gallery = atob(search.get("g")).split(',');

        gallery.forEach(x => addToGallery(x, true));

        url = localStorage.getItem("url");
        if(!url && gallery.length) url = gallery[0];
        $("#url").val(url);
    }

    checkInput.bind($("#url"))(true);
});

function onVRViewLoad(urlToUse) {
    if(vrView) {
        vrView.setContent({
            image: urlToUse,
            is_autopan_off: true
        });
    }
    else {
        vrView = new VRView.Player('#vrview', {
            image: urlToUse,
            is_vr_off: false
        });
    }
    $("#vrview").addClass("loaded");
    $("body").addClass("vrview-loaded");
    $("#url").val(urlToUse);
    if(localStorageEnabled) localStorage.setItem("url", urlToUse);
    $("#vr-label").text(`${gallery.indexOf(urlToUse) + 1} / ${gallery.length}`)
    scrollToView();
}

function checkInput(debounced = false) {
    if(debounced === false) {
        if(debounce) {
            clearTimeout(debounce);
        }
        debounce = setTimeout(() => checkInput.bind(this)(true), 500);
        return debounce;
    }
    try {
        url = new URL($(this).val()).href;
        addToGallery(url);
        onVRViewLoad(url);
    } 
    catch (_) {
        console.error(_);
        return;
    }
}

function addToGallery(urlToAdd, automated = false) {
    if(!gallery.includes(urlToAdd)) {
        gallery.push(urlToAdd.replace(',', '%2C'));
    }
    else if(!automated) {
        return;
    }
    let child = $("#gallery").append(`<div class="gallery-item shadow rounded mb-4 mt-3 mx-1"><img class="rounded" src="${urlToAdd}"/></div>`).children().last();
    child.on("click", function() {
        console.log(this);
        url = gallery[this];
        onVRViewLoad(gallery[this]);
    }.bind($("#gallery").children().length - 1)); // Use this instead of gallery.length to circumvent backloading bug

    if(gallery.length > 1) {
        $("#vrview").addClass("gallery");
    }

    if(localStorageEnabled && !automated) {
        localStorage.setItem("gallery", gallery);
    }
}

function prevGallery() {
    let index = gallery.indexOf(url);
    if(index == 0) {
        index = gallery.length - 1;
    }
    else {
        index--;
    }
    url = gallery[index];
    onVRViewLoad(url);
}

function nextGallery() {
    let index = gallery.indexOf(url);
    if(index == gallery.length -1) {
        index = 0;
    }
    else {
        index++;
    }
    url = gallery[index];
    onVRViewLoad(url);
}

$("#url").on('input', function() { checkInput.bind(this)() });

function scrollToView() {
    $(window).scrollTop($("#vrview").offset().top + 1); // don't ask why it needs the +1, for some reason the gallery breaks this
}

/* https://stackoverflow.com/a/51586232 CC-BY-SA @emanuel

document.onpaste = function(event){
    var items = (event.clipboardData || event.originalEvent.clipboardData).items;
    for (index in items) {
      var item = items[index];
      if (item.kind === 'file') {
        var blob = item.getAsFile();
        var reader = new FileReader();
        reader.onload = function(event){
            url = event.target.result;
            $("#url").val("{pasted image}")
            onVRViewLoad();
        }
        reader.readAsDataURL(blob);
      }
    }
}*/

$(window).scroll(function() {
    if($(window).scrollTop() >= $("#vrview").offset().top) console.log("true")
})

function clearGallery() {
    if(window.location.search) {
        let u = new URL(window.location.href);
        u.searchParams.delete("g");
        window.location.href = u.href;
    }
    gallery = [];
    if(localStorageEnabled) {
        localStorage.clear();
        localStorage.setItem("cookies", true);
    }
    url = "";
    $("#url").val("");
    vrView = null;
    $("#vrview iframe").remove();
    $("#gallery").empty();
}

function share() {
    const url = new URL(window.location.href);
    url.searchParams.set("g", btoa(gallery.join(',')));
    navigator.clipboard.writeText(url.href);
    $("#shareToast").toast("show");
}

function acceptCookies() {
    localStorage.setItem("cookies", true);
    localStorageEnabled = true;
    localStorage.setItem("gallery", gallery);
    localStorage.setItem("url", url);
    $("#cookieToast").toast("hide");
}

function denyCookies() {
    $("#cookieToast").toast("hide");
}
